#List the registration fields from the argument file in a pretty way
# USE: pretyPrintRegistry filein fileout
#!/usr/bin/python
import sys
if  3 == len(sys.argv):
    out = open(sys.argv[2], "w")
    file = open(sys.argv[1],"r");
    reg = file.readlines();
    for line in reg:
        regEntry = line.split(":");
        out.write(regEntry[0]+":\n");
        for field in regEntry[1:]:
            out.write("\t" + field + "\n")
    file.close();
elif 2 == len(sys.argv):
    file = open(sys.argv[1],"r");
    reg = file.readlines();
    for line in reg:
        regEntry = line.split(":");
        print regEntry[0]+":";
        for field in regEntry[1:]:
            print "\t" + field
    file.close();
else:
    print "Wrong arguments"
    sys.exit(2)

#HOWTO: run translate.py then open in browser 127.0.0.1:8081 to see the results
#!/usr/bin/python
# coding=utf-8

translate ={
	"Música" : "Music",
	"Pasado" : "Past",
	"primer" : "first",
	"jueves" : "thursday",
	"Contacto" : "Contact",
	"Planos" : "Maps",
	"Perfil" : "Profile",
	"Cómo" : "How",
	"noticias" : "news",
	"Disfruta" : "Enjoy",
	"Universidad" : "University"
}
def translate10():
	import string, cgi, time;
	from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer;
	from urllib2 import URLError, HTTPError, urlopen;
	class my_server(BaseHTTPRequestHandler):
		def do_GET(self):
			address="http://www.upm.es"+self.path;
			url=urlopen(address);
			data=url.read();
			tipo=url.info().getheader("Content-Type");
			for k, v in translate.iteritems():
				data = data.replace(k,v);
			self.send_response(200);
			self.send_header("Content-type", tipo );
			self.end_headers();
			self.wfile.write(data);
			print self.client_address[0];
			def log_message(self, format, *args):
				return
	server=HTTPServer(("",8081),my_server);
	server.serve_forever();
translate10();